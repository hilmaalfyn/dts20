<html>
<head>
    <title>Fungsi dan Prosedur</title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body>
	
    <h3>Menghitung Luas dan Keliling Persegi</h2>
    <form action="./" method="POST" enctype="multipart/form-data">
        <table>
            <tr>
                <td>Panjang</td>
                <td>:</td>
                <td><input type="text" name="panjang" required ></td>
            </tr>
            <tr>
                <td>Lebar</td>
                <td>:</td>
                <td><input type="text" name="lebar" required ></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" name="submit" value="Hitung"></td>
            </tr>
        </table>
    </form>
    <?php
    if(isset($_POST['submit'])){
        $panjang    =$_POST['panjang'];
        $lebar      =$_POST['lebar'];
               
        // menghitung luas persegi panjang
        $luasPP = $panjang*$lebar;
        
        // menghitung keliling persegi panjang
        $kelilingPP = 2*($panjang+$lebar);
                
        echo "Keterangan :<br />";
        echo "Panjang = $panjang<br />";
        echo "Lebar = $lebar<br />";
        echo "Luas Persegi Panjang [ $panjang x $lebar ] = $luasPP<br />";
        echo "Keliling Persegi Panjang [ 2 x ($panjang + $lebar) ] = $kelilingPP";
    }
?>

</body>
</html>