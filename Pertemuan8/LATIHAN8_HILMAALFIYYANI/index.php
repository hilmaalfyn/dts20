<!DOCTYPE html>
<html>
<head>
	<title>Fungsi dan Prosedur PHP</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="style.css">
</head>
<body>

	

	<div class="container">
    <form action="./" class="form" method="POST" enctype="multipart/form-data">
      <h3><center>Hitung Luas & Keliling</center></h3>
      <h3><center>Persegi Panjang</center></h3><br>
      <div class="input-field">
        <label for="text">Panjang</label>
        <input type="text" name="panjang" required>
      </div>
      <div class="input-field">
        <label for="text">Lebar</label>
        <input type="text"  name="lebar" required>
      </div>
      <input type="submit" name="submit" value="Hitung">
     </form> 
     <?php
    if(isset($_POST['submit'])){
        $panjang = $_POST['panjang'];
        $lebar =$_POST['lebar'];
               
        // menghitung luas persegi panjang
        $luasPP = $panjang*$lebar;

        // menghitung keliling persegi panjang
        $kelilingPP = 2*($panjang+$lebar);
                
        echo "<br />";
        echo "Keterangan :<br />";
        echo "Panjang = $panjang<br />";
        echo "Lebar = $lebar<br />";
        echo "Luas Persegi Panjang = <br> [ $panjang x $lebar ] = $luasPP<br />";
        echo "Keliling Persegi Panjang = <br> [ 2 x ($panjang + $lebar) ] = $kelilingPP";
    }
    ?>
  </div>

</body>
</html>

