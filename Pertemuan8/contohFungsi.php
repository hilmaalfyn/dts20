<!DOCTYPE html>
<html>
<head>
	<title>Fungsi PHP</title>
</head>
<body>

	<?php

		//fungsi
		//function perkenalan(){
		//	echo "Selamat Datang <br>";
		//	echo "Pada Acara DIGITAL TALENT 2020";
		//}
		//perkenalan();

		//fungsi dgn parameter
		//function perkenalan($salam, $nama){
		//	echo $salam . ", ";
		//	echo "Perkenalkan saya ".$nama. "<br>";
		//	echo "Senang Berkenalan dengan Anda";
		//}
		//perkenalan("Halo", "Hilma");

		//fungsi dgn parameter default
		//function perkenalan( $nama, $salam="Selamat Pagi"){
		//	echo $salam . ", ";
		//	echo "Perkenalkan saya ".$nama. "<br>";
		//	echo "Senang Berkenalan dengan Anda";
		//}
		//$saya = "Hilma";
		//perkenalan($saya);

		//fungsi mengembalikan nilai (return)
		//function hitungUmur($thnLahir, $thnSekarang){
		//	$umur = $thnSekarang - $thnLahir;
		//	return $umur;
		//}
		//echo "Umur saya adalah ".hitungUmur(2002, 2020)." Tahun";

		//memanggil fungsi didalam fungsi lain
		//function hitungUmur($thnLahir, $thnSekarang){
		//	$umur = $thnSekarang - $thnLahir;
		//	return $umur;
		//}

		//function perkenalan( $nama, $salam="Selamat Pagi"){
		//	echo $salam . ", ";
		//	echo "Perkenalkan saya ".$nama. "<br>";
		//	echo "Umur saya ".hitungUmur(2002, 2020)." Tahun<br>";
		//	echo "Senang Berkenalan dengan Anda";
		//}
		//perkenalan("Hilma");

		//fungsi rekursif
		//function faktorial($angka){
		//	if($angka < 2){
		//		return 1;
		//	} else {
		//		//memanggil dirinya sendiri
		//		return ($angka * faktorial($angka-1));
		//	}
		//}
		//memanggil fungsi
		//echo "Faktorial 5 adalah ".faktorial(5);

		//contoh prosedur
		function do_print(){
			//mencetak informasi timestamp
			echo time();
		}

		//memanggil prosedur
		do_print();
		echo '<br/>';

		//contoh fungsi penjumlahan
		function jumlah($a, $b){
			return ($a + $b);
		}
		echo jumlah(2, 3);


	?>

</body>
</html>