<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel ="stylesheet" href ="css/bootstrap.min.css">
</head>

<body>
    <div class = "col">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <h2>INPUT FORM</h2>
      </nav>
    </div>

  <div class ="col">
    <form action="cek.php" method="post">
      <div class="form-group">
        <label for="nama"><b>Nama</b></label>
        <input type="text" class="form-control" id="nama" name="nama"  placeholder="Nama Lengkap (sesuai KTP)" required>
      </div>
      <div class="form-group">
        <label for="alamat"><b>Alamat</b></label>
        <textarea class="form-control" id="alamat" rows="2" name="alamat" placeholder="Alamat Lengkap" required></textarea>
      </div>
      <div class="form-group">
      <div class="row">
        <div class="col">
            <label for="tempat"><b>Tempat Lahir</b></label>
            <input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat Lahir" required>
        </div>
        <div class="col">
            <label for="tanggal"><b>Tanggal Lahir</b></label>
            <input type="date" class="form-control" id="tgl" name="tanggal" required>
        </div>
      </div>
      </div>
      <div class="form-group" data-toggle="validator">
        <label for="jenkel"><b>Jenis Kelamin</b></label>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="jenkel" id="lk" value="Laki-Laki" required>
          <label class="form-check-label" for="jenkel">Laki-Laki</label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="jenkel" id="pr" value="Perempuan" required>
          <label class="form-check-label" for="jenkel">Perempuan</label>
        </div>
      </div>
      <div class="form-group">
      <div class="row">
        <div class="col-md-2">
          <label for="umur"><b>Umur</b></label>
          <input type="number" class="form-control" id="umur" name="umur" placeholder="Isi dengan angka" required>
          <small class="form-text text-muted"> *Tahun</small>
        </div>
      </div>
      </div>
      <button type="submit" class="btn btn-primary">Kirim</button>
    </form>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>