<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Project</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            var method = '';
            $("#form-modal").on("hide.bs.modal", function () {
                $("#form").trigger("reset");
            });

            $("#form").submit(function(e){
                e.preventDefault();
                $.ajax({
                    url: 'service.php?action='+method,
                    type: 'post',
                    data: $(this).serialize(),
                    success: function(data) {
                        $('#form-modal').modal('hide');
                        $("#table").load("index.php #table");
                    }
                });
            });

            $(document).on("click",".hapus", function(){
                $.ajax({
                    url: 'service.php?action=hapus&id='+$(this).attr("value"),
                    type: 'get',
                    success: function(data) {
                        $("#table").load("index.php #table");
                    }
                });
            });

            $(document).on("click",".ubah", function(){
                $("input[name=id]").val($(this).attr("value"));
                $("input[name=menu]").val($(this).closest("tr").find("td:eq(1)").text());
                $("input[name=deskripsi]").val($(this).closest("tr").find("td:eq(2)").text());
                $("input[name=kategori]").val($(this).closest("tr").find("td:eq(3)").text());
                $("input[name=harga]").val($(this).closest("tr").find("td:eq(4)").text());

                $('#form-modal').modal('show');
                method = 'ubah';
            });

            $("#tambah").click(function(){
                method = 'simpan';
            });

        });
    </script>

</head>

<body>

    <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand mb-0 h1" href="">DTSRESTO</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="home.php">BERANDA</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">DAFTAR MENU <span class="sr-only">(current)</span></a>
                    </li>
            </div>
            <button type="button" id="tambah" class="btn btn-primary mt-2 mb-2" data-toggle="modal" data-target="#form-modal"><i class="fa fa-plus"></i> Menu Baru
            </button>
        </nav>
        
        <div class="col">
        <table class="table" id="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">No</th>
                <th scope="col">MENU</th>
                <th scope="col">DESKRIPSI</th>
                <th scope="col">KATEGORI</th>
                <th scope="col">HARGA</th>
                <th scope="col">AKSI</th>
                </tr>
            </thead>
            <?php
                include "koneksi.php";
                $sql = "SELECT * FROM menu ORDER BY id";
                $hasil = mysqli_query($db,$sql);
                foreach ($hasil as $key => $data) {
            ?>
                <tbody>
                    <tr>
                        <td><?php echo $key + 1 ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><?php echo $data['deskripsi']?></td>
                        <td><?php echo $data['kategori']?></td>
                        <td><?php echo $data['harga']?></td>
                        <td>
                            <button type="button" class="btn btn-warning ubah" value="<?php echo $data['id']?>"><i class="fa fa-pencil"></i> Ubah</button>
                            <button type="button" class="btn btn-danger hapus" value="<?php echo $data['id']?>"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                </tbody>
            <?php
                }
            ?>
        </table>
    </div>
        

        <!-- Modal Form -->
        
        <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="" id="form">
                    <input type="hidden" name="id">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="nama">Menu</label>
                                <input type="text" name="menu" placeholder="Masukkan nama menu" class="form-control" id=""  required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <input type="text" name="deskripsi" placeholder="Masukkan deskripsi menu" class="form-control" id="" required>
                            </div>
                            <div class="form-group">
                                <label for="kategori">Kategori</label>
                                <input type="text" name="kategori" placeholder="Masukkan kategori menu" class="form-control" id="" required>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="number" name="harga" placeholder="Masukkan harga hanya angka" class="form-control" id="" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary mr-auto">Simpan</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close">Kembali</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Form -->

</body>
</html>
