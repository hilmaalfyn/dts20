<!DOCTYPE html>
<html>
<head>

	<title>Tambah Produk</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="css/bootstrap.min.css">
  	<script type="text/javascript">
    	function eraseText() {
     		document.getElementById("merek").value = "";
     		document.getElementById("warna").value = "";
     		document.getElementById("stok").value = "";
     		document.getElementById("satuan").value = "";
     		document.getElementById("harga").value = "";
		}
	</script>

</head>

<body>

	<?php
		include "koneksi.php";

		//START simpan ke database
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$merek  = $_POST["merek"];
			$warna  = $_POST["warna"];
			$stok   = $_POST["stok"];
			$satuan = $_POST["satuan"];
			$harga  = $_POST["harga"];
			$sql = "INSERT into produk (merk, warna, stok, satuan, harga) VALUES ('$merek','$warna','$stok','$satuan','$harga')";

			// START eksekusi data
			$hasil = mysqli_query($db, $sql);
			// END eksekusi data

			// START hasil eksekusi
			if ($hasil) {
				header("Location:index.php");
			} else {
				echo "<div class='alert alert-danger'>DATA GAGAL DISIMPAN</div>";
			} 
			// END hasil eksekusi
		}
		//END simpan ke database
	?>


    <div class = "col">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <h2>Tambah Produk</h2>
      </nav>
    </div>

  	<div class ="col">
    <form action="create.php" method="post">
      <div class="form-group">
        <label for="merek"><b>Merek</b></label>
        <input type="text" class="form-control" id="merek" name="merek"  placeholder="Merek produk" required>
      </div>
      <div class="form-group">
        <label for="warna"><b>Warna</b></label>
        <input type="text" class="form-control" id="warna" name="warna"  placeholder="Warna produk" required>
      </div>
      <div class="form-group">
        <label for="stok"><b>Stok</b></label>
        <input type="number" class="form-control" id="stok" name="stok"  placeholder="Isi dengan angka" required>
      </div>
      <div class="form-group">
        <label for="satuan"><b>Satuan</b></label>
        <input type="number" class="form-control" id="satuan" name="satuan"  placeholder="Isi dengan angka" required>
      </div>
      <div class="form-group">
        <label for="harga"><b>Harga</b></label>
        <input type="number" class="form-control" id="harga" name="harga"  placeholder="Isi dengan angka" required>
      </div>

      <button type="submit" class="btn btn-primary">Simpan</button>
      <button class="btn btn-warning" onclick="javascript:eraseText()">Ulangi</button>
      <a href="index.php" class="btn btn-secondary">Kembali</a>
    </form>
  	</div>

</body>
</html>