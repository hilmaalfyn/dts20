<!DOCTYPE html>
<html>
<head>

	<title>Ubah Produk</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="css/bootstrap.min.css">
  	<script type="text/javascript">
    	function eraseText() {
     		document.getElementById("merek").value = "";
     		document.getElementById("warna").value = "";
     		document.getElementById("stok").value = "";
     		document.getElementById("satuan").value = "";
     		document.getElementById("harga").value = "";
		}
	</script>

</head>

<body>

	<?php
		include "koneksi.php";

		//START simpan ke database
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$id 	  = $_POST["id"];
			$merek  = $_POST["merek"];
			$warna  = $_POST["warna"];
			$stok   = $_POST["stok"];
			$satuan = $_POST["satuan"];
			$harga  = $_POST["harga"];
			$sql = "UPDATE produk SET merk='$merek', warna='$warna', stok='$stok', satuan='$satuan', harga='$harga' WHERE id=$id";

			// START eksekusi data
			$hasil = mysqli_query($db, $sql);
			// END eksekusi data

			// START hasil eksekusi
			if ($hasil) {
				header("Location:index.php");
			} else {
				echo "<div class='alert alert-danger'>DATA GAGAL DIUBAH</div>";
			} 
			// END hasil eksekusi
		}
		//END simpan ke database

		//START GET data update
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
			
			$sql = "SELECT * FROM produk where id=$id";
			$hasil = mysqli_query($db, $sql);
			$data = mysqli_fetch_assoc($hasil);
		}
		//END GET data update
	?>


    <div class = "col">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <h2>Ubah Produk</h2>
      </nav>
    </div>

  	<div class ="col">
    <form action="<?php echo($_SERVER['PHP_SELF']) ?>" method="post">
    	<input type="hidden" name="id" value="<?php echo $data['id'] ?>">
      <div class="form-group">
        <label for="merek"><b>Merek</b></label>
        <input type="text" class="form-control" id="merek" name="merek"  placeholder="Merek produk" value="<?php echo $data['merk'] ?>" required>
      </div>
      <div class="form-group">
        <label for="warna"><b>Warna</b></label>
        <input type="text" class="form-control" id="warna" name="warna"  placeholder="Warna produk" value="<?php echo $data['warna'] ?>" required>
      </div>
      <div class="form-group">
        <label for="stok"><b>Stok</b></label>
        <input type="number" class="form-control" id="stok" name="stok"  placeholder="Isi dengan angka" value="<?php echo $data['stok'] ?>" required>
      </div>
      <div class="form-group">
        <label for="satuan"><b>Satuan</b></label>
        <input type="number" class="form-control" id="satuan" name="satuan"  placeholder="Isi dengan angka" value="<?php echo $data['satuan'] ?>" required>
      </div>
      <div class="form-group">
        <label for="harga"><b>Harga</b></label>
        <input type="number" class="form-control" id="harga" name="harga"  placeholder="Isi dengan angka" value="<?php echo $data['harga'] ?>" required>
      </div>

      <button type="submit" class="btn btn-primary">Simpan</button>
      <button class="btn btn-warning" onclick="javascript:eraseText()">Ulangi</button>
      <a href="index.php" class="btn btn-secondary">Kembali</a>
    </form>
  	</div>

</body>
</html>