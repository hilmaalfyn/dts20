<?php
session_start();
require 'koneksi.php';  

if(isset($_POST["login"])) {
	
	$username = $_POST["username"];
	$password = $_POST["password"];

	$result = mysqli_query($db, "SELECT * FROM user WHERE username = '$username'");

	//cek username ada atau tidak
		if(mysqli_num_rows($result) === 1) {

			//cek password
			$row = mysqli_fetch_assoc($result);
			if(password_verify($password, $row["password"])) {
				//set session
				$_SESSION["login"] = true;


				header("Location:index.php");
				exit;
			}
		}

		$error = true;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="css/bootstrap.min.css">
</head>
<body>

<div class="container">
<h1>Halaman Login</h1>

<?php if(isset($error)) : ?>  
	<p style = "color: red; font-style: italic;">username / password salah</p>
<?php endif; ?>	

<form action="" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Username</label>
    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary" name="login">Login</button>
  <a href="register.php" class="btn btn-primary">Register</a>
</form>

</div>
</body>
</html>