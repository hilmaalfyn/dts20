<?php
session_start();
require 'koneksi.php';

if( !isset($_SESSION["login"]) ) {
	header("Location:login.php");
}

?>


<!DOCTYPE html>
<html>
<head>
	<title>Daftar Produk</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="css/bootstrap.min.css">
</head>
<body>

	<div class = "col">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <h2>Daftar Produk</h2>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item active">
		        <a class="btn btn-danger" href="login.php">Logout</a>
		      </li>
		    </ul>
		</div>
      </nav>
    </div>

  	<div class ="col">
	<table id="example" class="table table-striped table-bordered" style="width:100%">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">NO.</th>
	      <th scope="col">MEREK</th>
 	      <th scope="col">WARNA</th>
	      <th scope="col">STOK</th>
	      <th scope="col">SATUAN</th>
	      <th scope="col">HARGA</th>
	      <th scope="col">AKSI</th>
	    </tr>
	  </thead>

	  <?php
	  	include "koneksi.php";

	  	$sql = "SELECT * from produk ORDER BY id";

	  	// START eksekusi data
	  	$hasil = mysqli_query($db, $sql);
	  	$no = 1 ;
	  	while ($data = mysqli_fetch_array($hasil)){
	  	// END eksekusi data
	  ?>

	  <!-- START isi table -->
	  <tbody>
	    <tr>
	    	<td><?php echo $no++?></td>
	    	<td><?php echo $data['merk']?></td>
	    	<td><?php echo $data['warna']?></td>
	    	<td><?php echo $data['stok']?></td>
	    	<td><?php echo $data['satuan']?></td>
	    	<td><?php echo $data['harga']?></td>
	    	<td>
	    		<a href="update.php?id=<?php echo $data['id']?>" class="btn btn-warning">Ubah</a>
	    		<a href="<?php echo $_SERVER['PHP_SELF']?> ?id=<?php echo $data['id']?>" class="btn btn-danger">Hapus</a>
	    	</td>
	    </tr>
	  </tbody>
	  <!-- END isi table -->

	  <?php
	  	}

	  	//hapus
	  	if (isset($_GET['id'])) {
	  		$id = $_GET['id'];
	  		$sql = "DELETE FROM produk WHERE id=$id";
	  		$hasil = mysqli_query($db, $sql);

	  		//kondisi berhasil atau tidak
	  		if ($hasil){
	  			header("Location:index.php");
	  		} else {
	  			echo "<div class='alert alert-danger'>DATA GAGAL DIHAPUS</div>";
	  		}
	  	}
	  ?>

	</table>
	<a href="create.php" class="btn btn-primary">Tambah Produk</a>
	</div>

</body>
</html>