<?php
require 'koneksi.php';
	
if(isset($_POST["register"])) {

	function registrasi($data) {
		global $db;

		$username = strtolower(stripslashes($data["username"]));
		$password = mysqli_real_escape_string($db, $data["password"]);
		$password2 = mysqli_real_escape_string($db, $data["password2"]);

		//cek username sudah ada atau belum
		$result = mysqli_query($db, "SELECT username FROM user WHERE username = '$username'");

		if (mysqli_fetch_assoc($result)) {
			echo "<script> alert('Username sudah terdaftar. COBA LAGI!') </script>";
			return false;
		}

		//cek konfirmasi password
		if ($password !== $password2) {
			echo "<script> alert('Konfirmasi password tidak sesuai') </script>";
			return false;
		}

		//enkripsi password
		$password = password_hash($password, PASSWORD_DEFAULT);

		//tambah user baru
		mysqli_query($db, "INSERT INTO user VALUES('', '$username', '$password')");

		return mysqli_affected_rows($db);
	}

	if(registrasi($_POST) > 0 ) {
		echo "<script>
				alert('User baru berhasil ditambahkan!');
				document.location.href = 'login.php';
			 </script>";
	} else {
		echo mysqli_error($db);
	}
}	

?>

<!DOCTYPE html>
<html>
<head>
	<title>Halaman Register</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel ="stylesheet" href ="css/bootstrap.min.css">
	<style>
		label {
			display: block;
		}
	</style>
</head>
<body>

<div class="container">
<h1>Halaman Register</h1>

<form action="" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Username</label>
    <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Konfirmasi Password</label>
    <input type="password" name="password2" class="form-control" id="exampleInputPassword1" required>
  </div>
  <button type="submit" name="register" class="btn btn-primary">Register</button>
</form>

</div>
</body>
</html>